# md: The Metadata Program

md simply allows you to view and/or edit the metadata of files.

Note: This project is in beta. The viewing functionality, although quite limited, works pretty well; editing metadata is not yet implemented.

## Developing

The CLI is rather simple. Just do the standard `cargo build` or `cargo run` to run the binary and provide options.

If you run it without options, you'll be warned that you didn't provide the `FILE` flag, indicated by `-f`.

If you don't pass an "action" flag, you'll also be warned.

An "action" in this context is some operation on the `FILE`. Right now, viewing with `-v` is the only available option.

Note that you must pass `-v` before `-f` if you include them in the same flag.

That is, the following is **valid**:

```
md -vf /path/to/file
```

while the following is **invalid**:

```
md -fv /path/to/file
```

You can, of course, pass flags separately in any order. Therefore, the following is totally valid:

```
md -f /path/to/file -v
```

The `long` versions to the commands (`--file`, for instance) were added only as a visual consistency to the help menu. Clap includes long versions for help and version by default.