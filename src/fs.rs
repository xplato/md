use std::fs;
use std::process;
use std::io::{ErrorKind};

use chrono::offset::Utc;
use chrono::DateTime;


// Prints the metadata to the path
pub fn view_metadata(path: &str) {
	// Returns a Metadata _viewer_ object
	let m = get_metadata(path);

	let prompt = if m.is_dir() {
		"Folder"
	} else {
		"File"
	};

	// Let's print the path that the program
	// received, just in case.
	println!("\n{}: {}", prompt, path);

	// If the time couldn't be gathered for
	// whatever reason, just skip it.
	if let Ok(time) = m.created() {
		let datetime: DateTime<Utc> = time.into();
		println!("Created: {}", fmt_time(datetime));
	}
	
	if let Ok(time) = m.modified() {
		let datetime: DateTime<Utc> = time.into();
		println!("Last modified: {}", fmt_time(datetime));
	}

	// We _could_ format this more nicely;
	// for instance, converting the sizes
	// to more friendly values. This is fine
	// for now, however.
	println!("Length: {} bytes", m.len());

	println!("Is Directory: {}", m.is_dir());

	// Check path permissions
	let mp = m.permissions();
	println!("Read only: {}", mp.readonly());
}

// Take a DateTime object and converts
// it into a time-formatted string.
// e.g.: 2022-04-02 18:57:25
fn fmt_time(datetime: DateTime<chrono::Utc>) -> String {
	return datetime.format("%Y-%m-%d %T").to_string();
}

// Fetch the Metadata object itself from
// the specified file.
fn get_metadata(file: &str) -> fs::Metadata {
	let metadata = fs::metadata(file);

	match metadata {
		Ok(data) => data,
		Err(err) => {
			let k = err.kind();

			if k == ErrorKind::NotFound {
				println!("No such file or directory.");
				process::exit(1);
			} else if k == ErrorKind::PermissionDenied {
				println!("Permission denied trying to read file.");
				process::exit(1);
			} else {
				println!("Unable to read file.");
				process::exit(1);
			}
		},
	}
}